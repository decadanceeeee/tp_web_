
$('.js-vote').click(function (ev) {
    ev.preventDefault();
    var $this = $(this),
        qid = $this.data('qid'),
        aid = $this.data('aid');

    $.ajax('/vote/', {
        method: 'POST',
        data: {
            qid: qid,
            aid: aid
        }
    }).done(function (data) {
        console.log("Response: ", data);
    });
});

$('.js-correct').click(function (ev) {
    ev.preventDefault();
    var $this = $(this),
        aid = $this.data('aid'),
        qid = $this.data('qid')

    $.ajax('/correct/', {
        method: 'POST',
        data: {
            aid: aid,
            qid: qid
        }
    }).done(function (data) {
        console.log("Response: ", data);
    });
});