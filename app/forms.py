from django import forms
from app import models
import random


class LoginForm(forms.Form):
    login = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class AnswerForm(forms.ModelForm):
    class Meta:
        model = models.Answer
        fields = ['text']

    text = forms.Textarea()


class SignUpForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password_one = forms.CharField(widget=forms.PasswordInput())
    password_repeat = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super().clean()
        password_one = cleaned_data['password_one']
        password_two = cleaned_data['password_repeat']

        if (password_two != password_one):
            self.add_error(None, 'passwords does not match')

        if models.User.objects.filter(email=self.cleaned_data['email']).count() != 0:
            self.add_error(None, 'email is already registered')

        if models.User.objects.filter(username=self.cleaned_data['username']).count() != 0:
            self.add_error(None, 'username is already registered')

        return cleaned_data

    def save(self, commit=True):
        user = models.User(username=self.cleaned_data['username'], email=self.cleaned_data['email'], password=self.cleaned_data['password_one'])
        profile = models.Profile(user=user, rating=0, avatar='ava.png')

        if (commit):
            user.save()
            profile.save()

        return user, profile


class SettingsForm(forms.ModelForm):
    avatar = forms.ImageField()

    class Meta:
        model = models.User
        fields = ['first_name', 'last_name', 'avatar']

    def save(self, *args, **kwargs):
        user = super().save(*args, **kwargs)
        user.profile.avatar = self.cleaned_data['avatar']
        user.profile.save()
        return user




class QuestionForm(forms.ModelForm):
    class Meta:
        model = models.Question
        fields = ['title', 'text', 'tag']

    title = forms.CharField()
    text = forms.Textarea()
    tag = forms.CharField()

