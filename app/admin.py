from django.contrib import admin

# Register your models here.

from app.models import Question, Answer, Profile, Answerlikes, Questionlikes, Tag

admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Profile)
admin.site.register(Answerlikes)
admin.site.register(Questionlikes)
admin.site.register(Tag)
