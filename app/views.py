from django.shortcuts import render, redirect, reverse
from django.core.paginator import Paginator
from django.db.models import Count, F
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.http import JsonResponse

from app import models
from app import forms

import random
from math import ceil

best_members = models.Profile.objects.order_by('-rating')[:10]
# popular_tags = models.Tag.question_set.count(num_likes='questionlikes__id').order_by('-num_likes')[:20]

popular_tags = models.Tag.objects.all().annotate(num_questions=Count('question__id')).order_by('-num_questions')[:30]


def paginate(objects_list, request, per_page=5):  # limit
    p = Paginator(objects_list, per_page)
    page = request.GET.get('page')

    objects_by_page = p.get_page(page)
    return objects_by_page


def new_questions(request):
    questions = models.Question.objects.new_questions()
    page_questions = paginate(questions, request)
    return render(request, 'index.html', {'questions': page_questions, 'popular_tags': popular_tags, 'best_members': best_members, 'pag_item': page_questions})


def hot(request):
    questions = models.Question.objects.best_questions()
    page_questions = paginate(questions, request)
    return render(request, 'hot_questions.html', {'questions': page_questions, 'popular_tags': popular_tags, 'best_members': best_members, 'pag_item': page_questions})


@login_required
def ask(request):
    if request.method == 'GET':
        form = forms.QuestionForm()
    else:
        form = forms.QuestionForm(data=request.POST)
        if form.is_valid():
            tags_str = form.cleaned_data.get('tag')
            tags = tags_str.split()

            question = form.save(commit=False)
            question.profile = request.user.profile
            question.save()

            for tag in tags:
                obj = models.Tag.objects.filter(name=tag).first()
                if obj is None:
                    new_tag = models.Tag(name=tag)
                    new_tag.save()
                    question.tag.add(new_tag)
                else:
                    question.tag.add(obj)
            return redirect(reverse('question', kwargs={'pk': question.pk}))
    return render(request, 'ask.html', {'form': form, 'popular_tags': popular_tags, 'best_members': best_members})


def tag(request, question_tag):
    questions = models.Question.objects.filter(tag__name=question_tag).alias(num_likes=Count('questionlikes__id')).order_by('-num_likes')
    page_questions = paginate(questions, request)
    return render(request, 'tag.html', {'questions': page_questions, 'current_tag': question_tag, 'popular_tags': popular_tags, 'best_members': best_members, 'pag_item': page_questions})


def login(request):
    if request.method == 'GET':
        form = forms.LoginForm()
        request.session['next'] = request.GET.get('next', '/new_questions/')
    else:
        form = forms.LoginForm(data=request.POST)
        if form.is_valid():
            user = auth.authenticate(request, username=form.cleaned_data.get('login'), password=form.cleaned_data.get('password'))
            if user is not None:
                auth.login(request, user)
                next = request.session.pop('next', '/new_questions/')[1:-1]
                return redirect(reverse(next))

    return render(request, 'login.html', {'form': form, 'popular_tags': popular_tags, 'best_members': best_members})



def question(request, pk):
    if request.method == 'GET':
        form = forms.AnswerForm()
    else:
        form = forms.AnswerForm(data=request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.question = models.Question.objects.get(id=pk)
            answer.profile = answer.question.profile
            answer.is_correct = False
            answer.save()
            page = ceil((answer.question.answer_set.count() + 3) // 5)
            return redirect(reverse('question', kwargs={'pk': models.Question.objects.get(id=pk).pk}) + '?page=' + str(page) + '#' + str(answer.id))

    question = models.Question.objects.get(id=pk)
    try:
        best_answer = question.answer_set.get(is_correct=True)
    except:
        best_answer = question.answer_set.alias(num_likes=Count('answerlikes__id')).order_by('-num_likes').first()
        best_answer.is_correct = True
        best_answer.save()

    answers = question.answer_set.alias(num_likes=Count('answerlikes__id')).order_by('-num_likes')
    page_answers = paginate(answers, request)
    return render(request, 'questionpage.html', {'form': form, 'one_question': question, 'best_answer': best_answer, 'answers': page_answers, 'popular_tags': popular_tags, 'best_members': best_members, 'pag_item': page_answers})


def register(request):
    if request.method == 'GET':
        form = forms.SignUpForm()
    else:
        form = forms.SignUpForm(data=request.POST)
        if form.is_valid():
            user, profile = form.save()
            auth.login(request, user)
            return redirect(reverse('new_questions'))
    return render(request, 'register.html', {'form': form, 'popular_tags': popular_tags, 'best_members': best_members})


@login_required
def settings(request):
    if request.method == 'GET':
        form = forms.SettingsForm()
    else:
        form = forms.SettingsForm(data=request.POST, files=request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect(reverse('settings'))

    profile = request.user.profile
    return render(request, 'settings.html', {'form': form, 'profile': profile, 'popular_tags': popular_tags, 'best_members': best_members})


@login_required
def logout(request):
    auth.logout(request)
    return redirect(reverse('new_questions'))


@login_required
@require_POST
def vote(request):
    data = request.POST
    qid = data.get('qid', '-1')
    if qid != -1:
        try:
            like = models.Questionlikes.objects.get(profile=request.user.profile,
                                                    question=models.Question.objects.get(id=qid))
            like.delete()
        except models.Questionlikes.DoesNotExist:
            like = models.Questionlikes(profile=request.user.profile, question=models.Question.objects.get(id=qid))
            like.save()
    aid = data.get('aid', '-1')
    if aid != -1:
        try:
            like = models.Answerlikes.objects.get(profile=request.user.profile, answer=models.Answer.objects.get(id=qid))
            like.delete()
        except models.Answerlikes.DoesNotExist:
            like = models.Answerlikes(profile=request.user.profile, answer=models.Answer.objects.get(id=qid))
            like.save()


    return JsonResponse(data)


@login_required
@require_POST
def correct(request):
    data = request.POST
    aid = data['aid']
    qid = data['qid']
    prev_correct = models.Question.objects.get(id=qid).answer_set.get(is_correct=True)
    prev_correct.is_correct = False
    prev_correct.save()
    cur = models.Question.objects.get(id=qid).answer_set.get(id=aid)
    cur.is_correct = True
    cur.save()

    return JsonResponse(data)
