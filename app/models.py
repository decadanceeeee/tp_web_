import datetime

from django.db import models
import os
from django.conf import settings
from django.db.models import Count
from django.contrib.auth.models import User
from django.utils import timezone


def images_path():
    return os.path.join(settings.STATICFILES_DIRS, 'img')


class Profile(models.Model):
    class Meta:
        app_label = 'app'

    rating = models.IntegerField()
    avatar = models.ImageField(upload_to='avatar/%Y/%m/%d', default='ava.png')

    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)

    AUTH_PROFILE_MODULE = 'app.Profile'


class QuestionManager(models.Manager):

    def new_questions(self):
        return Question.objects.order_by('-created_at')

    def best_questions(self):
        return Question.objects.all().annotate(num_likes=Count('questionlikes__id')).order_by('-num_likes')


class Tag(models.Model):
    name = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Question(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    profile = models.ForeignKey(Profile, null=True, on_delete=models.SET_NULL)
    tag = models.ManyToManyField(Tag)

    objects = QuestionManager()


class Answer(models.Model):
    text = models.TextField()
    is_correct = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    profile = models.ForeignKey(Profile, null=True, on_delete=models.SET_NULL)
    question = models.ForeignKey(Question, null=True, on_delete=models.CASCADE)


class Questionlikes(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    profile = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)
    question = models.ForeignKey(Question, on_delete=models.DO_NOTHING)


class Answerlikes(models.Model):  # unique_key
    created_at = models.DateTimeField(auto_now_add=True)

    profile = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)
    answer = models.ForeignKey(Answer, on_delete=models.DO_NOTHING)


