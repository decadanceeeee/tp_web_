from django.core.management.base import BaseCommand
from app.models import Question, Answer, Profile, Answerlikes, Questionlikes, Tag
from django.contrib.auth.models import User
from django.db import transaction

import random

import factory
import factory.django
from django.utils import timezone
from django.utils.timezone import make_aware

from random_username.generate import generate_username


NUM_USERS = 100
NUM_QUESTIONS = 100
NUM_ANSWERS = 1000
NUM_TAGS = 100

NUM_TAGS_PER_QUESTION = 2

NUM_LIKES_OF_QUESTIONS = 200
NUM_LIKES_OF_ANSWERS = 2000

usernames = list(set((generate_username(NUM_USERS))))
while (len(usernames) < NUM_USERS):
    usernames += generate_username(100)
    usernames = list(set(usernames))


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    rating = random.random()

    user = "user"


class TagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tag

    name = factory.Faker('word')
    created_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())
    updated_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())


class QuestionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Question

    title = factory.Faker('catch_phrase')
    text = factory.Faker('paragraph')
    created_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())
    updated_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())

    profile = factory.SubFactory(ProfileFactory)


class AnswerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Answer

    text = factory.Faker('paragraph')
    is_correct = False
    created_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())
    updated_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())

    profile = factory.SubFactory(ProfileFactory)


class QuestionlikesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Questionlikes

    created_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())

    profile = factory.SubFactory(ProfileFactory)
    question = factory.SubFactory(Question)


class AnswerslikesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Answerlikes

    created_at = factory.Faker('date_time', tzinfo=timezone.get_current_timezone())

    profile = factory.SubFactory(ProfileFactory)
    answer = factory.SubFactory(Answer)





class Command(BaseCommand):  # filng db dummy data

    def add_arguments(self, parser):
        parser.add_argument('--users',
            default=2,
            type=int,
            help='The number of fake users to create.')




    @transaction.atomic
    def handle(self, *args, **kwargs):
        self.stdout.write("Deleting old data...")
        models = [Question, Answer, Profile, Answerlikes, Questionlikes, Tag]
        for m in models:
            m.objects.all().delete()

        self.stdout.write("Creating new data...")
        people = []
        # Create all the users
        for _ in range(NUM_USERS):
            
            user = User(username=usernames[_])
            user.save()
            person = ProfileFactory(avatar='ava.png', rating=random.randint(1, 10000), user=user)
            people.append(person)

        # Tag generation
        tags = []
        for __ in range(NUM_TAGS):
            tag = TagFactory()
            tags.append(tag)

        questions = []
        answers = []
        # Questions and answers and tags per question generation
        for _ in range(NUM_QUESTIONS):
            question = QuestionFactory(profile=random.choice(people))
            questions.append(question)

            question_tags = random.choices(tags, k=2)
            question.tag.add(*question_tags)

        for _ in range(NUM_ANSWERS):
            answer = AnswerFactory(profile=random.choice(people), question=random.choice(questions))
            answers.append(answer)

        for _ in range(NUM_LIKES_OF_QUESTIONS):
            QuestionlikesFactory(profile=random.choice(people), question=random.choice(questions))

        for _ in range(NUM_LIKES_OF_ANSWERS):
            AnswerslikesFactory(profile=random.choice(people), answer=random.choice(answers))
