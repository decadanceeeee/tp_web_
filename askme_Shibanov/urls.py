"""askme_Shibanov URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.new_questions, name='new_questions'),
    path('hot/', views.hot, name='hot_questions'),
    path('settings/', views.settings, name='settings'),
    path('tag/<str:question_tag>/', views.tag, name='tag'),
    path('question/<int:pk>/', views.question, name='question'),
    path('login/', views.login, name='login'),
    path('signup/', views.register, name='signup'),
    path('ask/', views.ask, name='ask'),
    path('logout/', views.logout, name='logout'),

    path('vote/', views.vote, name='vote'),
    path('correct/', views.correct, name='correct'),

]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)